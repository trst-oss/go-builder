FROM registry.gitlab.com/trst-oss/nodejs:latest@sha256:6f791c438f4fc216f80f58eb31a5f1704745c234027f3b4ca60c986ae3dc6fb1

RUN apt-get install -y binutils gcc pkg-config curl gzip && rm -fr /var/cache/apt/archives/* /var/cache/apt/*.bin /var/log/* /var/cache/ldconfig/aux-cache

ENV GO_VERSION=1.24.0

RUN curl -L https://go.dev/dl/go${GO_VERSION}.linux-amd64.tar.gz | tar -zxv -C /opt/

ENV PATH="$PATH:/opt/go/bin:/root/go/bin"
