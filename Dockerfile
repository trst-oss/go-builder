FROM registry.gitlab.com/trst-oss/nodejs:latest@sha256:b03526675c2bb6c500aa76c528adb42e0ba055f309935201650786408601bcd8

RUN apt-get install -y binutils gcc pkg-config curl gzip && rm -fr /var/cache/apt/archives/* /var/cache/apt/*.bin /var/log/* /var/cache/ldconfig/aux-cache

ENV GO_VERSION=1.24.0

RUN curl -L https://go.dev/dl/go${GO_VERSION}.linux-amd64.tar.gz | tar -zxv -C /opt/

ENV PATH="$PATH:/opt/go/bin:/root/go/bin"
