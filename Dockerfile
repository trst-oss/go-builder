FROM registry.gitlab.com/trst-oss/nodejs:latest@sha256:89f3a396cfb10c2060386291bdb4b721b29be8b22345bbc7e9bcfbda506c2bb9

RUN apt-get install -y binutils gcc pkg-config curl gzip && rm -fr /var/cache/apt/archives/* /var/cache/apt/*.bin /var/log/* /var/cache/ldconfig/aux-cache

ENV GO_VERSION=1.24.1

RUN curl -L https://go.dev/dl/go${GO_VERSION}.linux-amd64.tar.gz | tar -zxv -C /opt/

ENV PATH="$PATH:/opt/go/bin:/root/go/bin"
